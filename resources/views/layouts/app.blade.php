<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'project latte') }}</title>

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="/assets/css/vendor.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/flat-admin.css">

  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="/assets/css/theme/blue-sky.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/theme/blue.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/theme/red.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/theme/yellow.css">

  <script>
  window.Laravel = <?php echo json_encode([
    'csrfToken' => csrf_token(),
    ]); ?>
  </script>
</head>
<body>
  <div class="app app-default">
    <aside class="app-sidebar" id="sidebar">
      <div class="sidebar-header">
        <a class="sidebar-brand" href="#"><span class="highlight">Project</span> Latte</a>
        <button type="button" class="sidebar-toggle">
          <i class="fa fa-times"></i>
        </button>
      </div>
      <div class="sidebar-menu">
        <ul class="sidebar-nav">
          <li class="active">
            <a href="./index.html">
              <div class="icon">
                <i class="fa fa-tasks" aria-hidden="true"></i>
              </div>
              <div class="title">Dashboard</div>
            </a>
          </li>
          <li>
            <a href="./index.html">
              <div class="icon">
                <i class="fa fa-building-o" aria-hidden="true"></i>
              </div>
              <div class="title">Places</div>
            </a>
          </li>
          <li>
            <a href="./index.html">
              <div class="icon">
                <i class="fa fa-users" aria-hidden="true"></i>
              </div>
              <div class="title">My Clients</div>
            </a>
          </li>
            <li>
            <a href="./index.html">
              <div class="icon">
                <i class="fa fa-thumb-tack" aria-hidden="true"></i>
              </div>
              <div class="title">My Appointments</div>
            </a>
          </li>
        </ul>
      </div>      
    </aside>
    <script type="text/ng-template" id="sidebar-dropdown.tpl.html">
    <div class="dropdown-background">
    <div class="bg"></div>
    </div>
    <div class="dropdown-container">
    
    </div>
    </script>
    <div class="app-container">
      <nav class="navbar navbar-default" id="navbar">
        <div class="container-fluid">
          <div class="navbar-collapse collapse in">
            <ul class="nav navbar-nav navbar-mobile">
              <li>
                <button type="button" class="sidebar-toggle">
                  <i class="fa fa-bars"></i>
                </button>
              </li>
              <li class="logo">
                <a class="navbar-brand" href="#"><span class="highlight">Project</span> Latte</a>
              </li>
            </ul>     
            <ul class="nav navbar-nav navbar-right">
              @if (Auth::guest())
              <li><a href="{{ url('/login') }}">Login</a></li>
              <li><a href="{{ url('/register') }}">Register</a></li>
              @else
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                  <li>
                    <a href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Logout
                  </a>

                  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>
              </ul>
            </li>
            @endif
          </ul>
        </div>
      </div>
    </nav>
     @yield('content')
    <footer class="app-footer"> 
      <div class="row">
        <div class="col-xs-12">
          <div class="footer-copyright">
            Copyright © 2016 Nugtem LLC & Billenium Club.
          </div>
        </div>
      </div>
    </footer>
  </div>
</div>
</body>
</html>
