@extends('layouts.app')

@section('content')
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <a class="card card-banner card-blue-light">
          <div class="card-body">
            <i class="icon fa fa fa-group fa-4x"></i>
            <div class="content">
              <div class="title">Appointents</div>
              <div class="value"><span class="sign"></span>100</div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <a class="card card-banner card-green-light">
          <div class="card-body">
            <i class="icon fa fa-check-circle fa-4x"></i>
            <div class="content">
              <div class="title">Completed</div>
              <div class="value"><span class="sign"></span>50</div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <a class="card card-banner card-orange-light">
          <div class="card-body">
            <i class="icon fa fa-exclamation-circle fa-4x"></i>
            <div class="content">
              <div class="title">Pending</div>
              <div class="value"><span class="sign"></span>50</div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <a class="card card-banner card-violet-light">
          <div class="card-body">
            <i class="icon fa fa fa-cogs fa-4x"></i>
            <div class="content">
              <div class="title">In Progress</div>
              <div class="value"><span class="sign"></span>12</div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <a class="card card-banner card-red-light">
          <div class="card-body">
            <i class="icon fa fa-minus-circle fa-4x"></i>
            <div class="content">
              <div class="title">Canceled</div>
              <div class="value"><span class="sign"></span>50</div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <a class="card card-banner card-yellow-light">
          <div class="card-body">
            <i class="icon fa fa-question-circle fa-4x"></i>
            <div class="content">
              <div class="title">Overdue</div>
              <div class="value"><span class="sign"></span>50</div>
            </div>
          </div>
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card card-mini">
          <div class="card-header">
            <div class="card-title">Pending</div>
            <ul class="card-action">
              <li>
                <a href="/">
                  <i class="fa fa-refresh"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="card-body no-padding table-responsive">   
            <table class="table card-table">
              <thead>
                <tr>
                  <th>Products</th>
                  <th class="right">Amount</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>MicroSD 64Gb</td>
                  <td class="right">12</td>
                  <td><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Complete</span></span></td>
                </tr>
                <tr>
                  <td>MiniPC i5</td>
                  <td class="right">5</td>
                  <td><span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Pending</span></span></td>
                </tr>
                <tr>
                  <td>Mountain Bike</td>
                  <td class="right">1</td>
                  <td><span class="badge badge-info badge-icon"><i class="fa fa-credit-card" aria-hidden="true"></i><span>Confirm Payment</span></span></td>
                </tr>
                <tr>
                  <td>Notebook</td>
                  <td class="right">10</td>
                  <td><span class="badge badge-danger badge-icon"><i class="fa fa-times" aria-hidden="true"></i><span>Cancelled</span></span></td>
                </tr>
                <tr>
                  <td>Raspberry Pi2</td>
                  <td class="right">6</td>
                  <td><span class="badge badge-primary badge-icon"><i class="fa fa-truck" aria-hidden="true"></i><span>Shipped</span></span></td>
                </tr>
                <tr>
                  <td>Flashdrive 128Mb</td>
                  <td class="right">40</td>
                  <td><span class="badge badge-info badge-icon"><i class="fa fa-credit-card" aria-hidden="true"></i><span>Confirm Payment</span></span></td>
                </tr>
              </tbody>
            </table>
            

          </div>
        </div>
      </div>  
    </div>
@endsection
